import java.util.Scanner;

public class BoardGameApp {
	
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		Board grid = new Board();

		int numCastles = 5;
		int turns = 0;
		
		System.out.println("Welcome to the Castle & Wall board game!!");
		
		boolean validInput = false;
		while (numCastles > 0 && turns < 8) {
			
			//printing board object, numCastles and turns
			System.out.println("Here is the board:");
			System.out.println(grid);
			System.out.println("You made " + turns + "out of 8"); 
			System.out.println("And you have " + numCastles + " castles left\n"); 
			
			//getting user input for position
			System.out.println("Enter a number (0-4) to set the col\n");
			int col = scanner.nextInt();
			System.out.println("Enter a number (0-4) to set the row\n");
			int row = scanner.nextInt();
			
			int response = grid.placeToken(row, col);
			
			if (response == -1 || response == -2) {
				System.out.println("\nPlease re-enter new col/row values");
			} else if (response == 1) {
				System.out.println("\nThere's a wall at this position");
				turns--;
			} else {
				System.out.println("\nThe castles tile is placed!");
				turns--;
				numCastles--;
			}
			System.out.println("\nHere is the grid after:");
			System.out.println(grid);
		}
		
		System.out.println("\nHere is the final board:");
		System.out.println(grid);
		
		//checks if player won or not
		if (numCastles == 0 ) {
			System.out.println("You won the game!!");
		} else {
			System.out.println("You lost the game :((");
		}
		
		System.out.println("Good game");
	}
}
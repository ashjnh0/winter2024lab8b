import java.util.Random;

public class Board {
	
	private Tile[][] grid;
	final int size = 5;
	Random rng = new Random();
	
	//constructor to initialize 5x5 arr
	public Board() {

		grid = new Tile[size][size];
		
		//setting it all to blanks
		for(int i = 0; i < grid.length; i++) {
			for(int j = 0; j < grid[i].length; j++) {
				grid[i][j] = Tile.BLANK;
			}
		}
		
		//setting hidden walls to rand positions
		for(int row = 0; row < grid.length; row++) {
			int col = rng.nextInt(grid[row].length);
			grid[row][col] = Tile.HIDDENWALL;
		}
		
	}
	
	//returns a string representation of the board
	@Override
	public String toString() {
		StringBuilder printBoard = new StringBuilder();
		
		for(int x = 0; x < grid.length; x++) {
			for( int s = 0; s < grid[x].length; s++) {
				printBoard.append(grid[x][s].getName()).append(" ");
			}
			printBoard.append("\n");
		}
		return printBoard.toString();
	}
	
	public int placeToken(int row, int col) {
		
		if(row > 5 || col > 5) {
			return -2;
		} 
		else if (grid[row][col] == Tile.CASTLE || grid[row][col] == Tile.WALL) {
			return -1;
		} else if (grid[row][col] == Tile.HIDDENWALL) {
			grid[row][col] = Tile.WALL;
			return 1;
		} else {
			grid[row][col] = Tile.CASTLE;
			return 0;
		}
		
	}
	
}
enum Tile {
	WALL("W"),
	CASTLE("C"),
	BLANK("_"),
	HIDDENWALL("_");
	
	private final String name;
	
	Tile(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}